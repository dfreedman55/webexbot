#! /usr/bin/env python

'''
Author: Dan Freedman (danfreed@cisco.com)
        Cloud Infrastructure & Software Group
        Technical Solutions Architect
'''

import time
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
import main


def on_created(event):
    time.sleep(5)
    text = ""
    pdf_filenames = main.get_pdfs()
    rooms = main.get_rooms()
    main.distribute_files(text, pdf_filenames, rooms)


# def on_deleted(event):
#     print(f"{event.src_path} has been deleted!")


# def on_modified(event):
#     print(f"{event.src_path} has been modified!")


# def on_moved(event):
#     print(f"{event.src_path} has been moved to {event.dest_path}")


if __name__ == "__main__":
    patterns = ["*.pdf"]
    ignore_patterns = None
    ignore_directories = True
    case_sensitive = True
    my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)

    my_event_handler.on_created = on_created
    # my_event_handler.on_deleted = on_deleted
    # my_event_handler.on_modified = on_modified
    # my_event_handler.on_moved = on_moved

    path = "."
    go_recursively = False
    my_observer = Observer()
    my_observer.schedule(my_event_handler, path, recursive=go_recursively)

    my_observer.start()
    print("Actively monitoring local directory for .pdf files")
    try:
        while True:
            time.sleep(15)
    except KeyboardInterrupt:
        my_observer.stop()
        my_observer.join()
