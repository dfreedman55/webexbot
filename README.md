# **PURPOSE**

The purpose of this application is to make it easy to distribute information to multiple Webex Team Rooms at the same time.

I typically email important news to my Cisco Partners, but with new-hires and attrition I don't always have all necessary email addresses.  In addition, some partners don't want me emailing their distribution list.  Instead, they prefer to receive information over our shared Webex Teams room (it's common to have a room for each partner).  Since I want to continue using email as my primary means of writing up and formatting data, I needed an easy way to leverage that content and to distribute that information to  Webex Team Rooms.

Solution :
1. Create Webex Bot
2. Add Webex Bot to Webex Team Room for each Cisco Partner
3. Print and save email(s) to .PDF file(s)
4. Query Webex API to determine which rooms Webex Bot is in
5. Send saved .PDF files to each room

---
# **DETAILED INSTRUCTIONS**

## Prerequisites
1. You must have [Python3](https://www.python.org/downloads/) installed on your machine
2. You must have [GIT](https://git-scm.com/downloads) installed on your machine

## Recommendations
If you're a Windows user, I highly recommend that you use GIT-BASH terminal emulator which comes installed with GIT.

GIT-BASH emulates a Linux terminal/CLI on a Windows machine which helps avoid command confusion when following the steps below.  For example, "touch" is not normally a valid command on Windows but if you're using GIT-BASH it works on Windows.  

> `GIT-BASH will make your life easier... use it when following the instructions below to ensure a smooth setup!`

## Clone Project Repository
```
git clone https://gitlab.com/dfreedman55/webexbot.git
```

## Create Webex Bot
[https://developer.webex.com/my-apps/new/bot](https://developer.webex.com/my-apps/new/bot)

## Create .env File
Create **.env** file in same directory as **main.py**.

Save Webex Bot details in your **.env** file following the examples below.

Only the `BOT_TOKEN` variable is used in the application today but you may wish to have the other details at some point in the future.

The **.gitignore** file ensures that the **.env** file will not sync to a remote GIT repository, so that you don't publicly expose your `BOT_TOKEN`.

Examples:
> PARENT_ACCOUNT="_<account_name_here>_"

> BOT_NAME="_<bot_name_here>_"

> BOT_EMAIL="_<bot_email_here>_"

> BOT_ID="_<bot_id_here>_"

> BOT_TOKEN="_<bot_token_here>_"

Keep the `"`s but remove the `<prompt>`s.

You can create multiple groups of the above variables in the same .env file, one group after the other.  You will be prompted during script execution which BOT you wish to use to send messages.  The appropriate `BOT_TOKEN` will be selected automatically.

## Add Webex Bot to Webex Teams room(s)
```
Room > People > Add Team Members > Search Bot Name > Add
```
In the Webex Desktop application, do this once per Partner Webex Team room.

## Print Outlook Email to .PDF
```
Email > File > Print > Printer > Microsoft Print to PDF
```
Create one .PDF file per email that you want to distribute.

Save the .PDF file(s) in the same directory as main.py.

Each file must have a unique name.

Name each file as _<unique_filename_here>_.**pdf**

> `case sensitive, use lowercase .pdf file extension`

## Create Virtual Environment
Use one of the below commands to create your virtual environment.

The proper command depends on your operating system.

```
py -3 -m venv .venv
```
* **Windows**: use this command when both Python2 & Python3 are installed

```
python3 -m venv .venv
```
* **Windows/macOS/Linux**: use this command when only Python3 is installed

## Activate Virtual Environment

Use one of the below commands to activate your virtual environment.

The proper command depends on your terminal emulator.
```
source .venv/Scripts/activate
```
* **Windows**: use this command when using Linux-based terminal emulators like GIT-BASH
```

source .venv\Scripts\activate
```
* **Windows**: use this command when using Windows-based terminal emulators

```
source .venv/bin/activate
```
* **macOS/Linux**: use this command

## Install Application Dependencies
```
pip install -r requirements.txt
```
Installs Python libraries needed to run the application (ex: `requests` library for making Python-based API calls).

## Run Application - Manual Mode
```
python main.py
```
You **must** always activate your virtual environment before running the application (see instructions above).

When running the application this way (manual mode), you will be able to add a personalized message.

The application will report status messages to the terminal window.

All .PDFs will be automatically renamed to include a timestamp and placed into the archive folder.

Mouse over each image below to understand the progession of execution.

![BeforeRun](/images/BeforeRun.JPG "Before Run")

![DuringRun](/images/DuringRun.JPG "During Run")

![AfterRun](/images/AfterRun.JPG "After Run")
---
# **OPTIONAL INSTRUCTIONS**

The watchdog (directory monitor) will continuously monitor the local directory every 15 seconds for the presence of any new .PDF files.  As soon as you save your email as .PDF to the local directory, the application will automatically send the files found to the Webex Teams room(s) that the Webex Bot has joined and archive the file with timestamp as already explained above.

## Run Application - Watchdog Mode

```
python watch.py &
```

When running the application this way (as a background process), you will **_not_** be able to add a personalized message.

The application will report status messages to the terminal window if you've kept it open.  If you close the terminal window, your background job will be terminated.

On Linux (GIT-BASH), the background process can be found with the 'jobs' command and terminated with this command: **kill %**_<job_id_here>_.

![RunWatchdog](/images/RunWatchdog.JPG "Run Watchdog")

---
# **THE END RESULT**

Below is what you should see in each Partner Webex Team Room, depending on the name and icon you gave to your BOT when you created it, and depending on name and specific contents of the .PDF file you sent.  If you ran the application manually and included a personalized message, that message will show up below the attachment in the image.

The receiver of the message can mouse over the preview to download the complete .PDF file as expected.

![EndResult](/images/EndResult.JPG "End Result")

![PDFPreview](/images/PDFPreview.JPG "PDF Preview")
