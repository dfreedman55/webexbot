#! /usr/bin/env python

'''
Author: Dan Freedman (danfreed@cisco.com)
        Cloud Infrastructure & Software Group
        Technical Solutions Architect
'''


# IMPORT MODULES
from requests_toolbelt.multipart.encoder import MultipartEncoder
from logging.handlers import RotatingFileHandler
from pathlib import Path
import requests
import datetime
import logging
import certifi
import json
import sys
import os


# SETUP FOLDERS AND FILES
if not os.path.exists("./archive"):
    os.mkdir("./archive")
if not os.path.exists("./logs"):
    os.mkdir("./logs")
if not os.path.exists("./logs/debug.log"):
    Path("./logs/debug.log").touch()


# SETUP LOGGING
logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)  # logger captures this level
logger_formatter = logging.Formatter(
    "%(asctime)s:%(name)s:%(module)s:%(funcName)s:%(levelname)s:%(message)s")
logger_file_handler = RotatingFileHandler(
    "./logs/debug.log",
    maxBytes=1024000,
    backupCount=3)
logger_file_handler.setLevel(logging.DEBUG)  # log this level to file
logger_file_handler.setFormatter(logger_formatter)
logger.addHandler(logger_file_handler)

logger.debug("\n================================\
================================================\n")


# DEFINE FUNCTIONS
def get_bots():
    ''' Return a list of tuples from .env file'''
    bot_name = ""
    bot_token = ""
    bots = []
    with open(".env", "r") as handle:
        lines = handle.readlines()
    for line in lines:
        line = line.strip("\n")
        if "BOT_NAME" in line:
            bot_name = line.split("=")[1].strip('"')
        if "BOT_TOKEN" in line:
            bot_token = line.split("=")[1].strip('"')
        if bot_name and bot_token:
            bots.append((bot_name, bot_token))
            bot_name = ""
            bot_token = ""
    return bots


def get_bot_token(bots):
    ''' Select bot and return its token '''
    print("Which bot would you like to use [enter a number]?")
    for index, bot in enumerate(bots):
        print(f"[{index}] {bot[0]}")
    bot_index = input()
    return bots[int(bot_index)][1]


def get_action():
    print("What do you want to do?")
    print("[0] Post text message")
    print("[1] Post text message and PDF attachment")
    action = input()
    if action == '0':
        action = 'text'
    else:
        action = 'text-pdf'
    return action


def get_personal_msg():
    ''' Define personalized message '''
    text = input("Enter personalized message [ENTER for none]: ")
    return text


def get_pdfs():
    ''' Get list of .PDF files in local directory '''
    for dirpath, dirnames, filenames in os.walk(os.curdir):
        if dirpath == os.curdir:
            pdf_filenames = [file for file in filenames if ".pdf" in file]
    if len(pdf_filenames) != 0:
        return pdf_filenames
    else:
        print("No .PDF files exist in local directory!")
        sys.exit(1)


def get_rooms(bot_token):
    ''' Get list of rooms Webex Bot is present in '''
    base_url = "https://webexapis.com"
    api_resource = "/v1/rooms"
    headers = {
        "Authorization": f"Bearer {bot_token}",
        "Content-Type": "application/json"
    }
    try:
        response = requests.get(
            f"{base_url}{api_resource}",
            headers=headers,
            verify=certifi.where()
        )
    except Exception as e:
        logger.exception(f"\nEXCEPTION: {e}\n\n")
        print(f"EXCEPTION: {e}")
        sys.exit(1)
    else:
        if response.ok:
            logger.info("\nSUCCESS: fetch room list\n")
            logger.debug(f"\nDETAILS: Room List {response.json()['items']}\n")
            return response.json()["items"]
        else:
            logger.error("\nFAILURE: fetch room list\n")
            print(
                f"[{response.status_code}]",
                json.loads(response.text).get("message")
            )
            sys.exit(1)


def post_text(room_id, room_name, bot_token, text=""):
    ''' Post .PDF file to room with or without personalized message '''
    base_url = "https://webexapis.com"
    api_resource = "/v1/messages"
    data = MultipartEncoder(
        {
            "roomId": room_id,
            "text": text,
        }
    )
    headers = {
        "Authorization": f"Bearer {bot_token}",
        "Content-Type": data.content_type
    }
    try:
        response = requests.post(
            f"{base_url}{api_resource}",
            headers=headers,
            data=data,
            verify=certifi.where()
        )
    except Exception as e:
        logger.exception(f"\nEXCEPTION: {e}\n\n")
        print(f"EXCEPTION: {e}")
        sys.exit(1)
    else:
        if response.ok:
            logger.info(f"\nSUCCESS: send file to room\n")
            logger.debug(f"\nDETAILS: {room_name}\n")
            print(F"SUCCESS: {room_name}")
        else:
            logger.error("\nFAILURE: send file to room\n")
            print(
                f"[{response.status_code}]",
                json.loads(response.text).get("message")
            )
            sys.exit(1)     


def post_text_pdf(filename, room_id, room_name, bot_token, text=""):
    ''' Post .PDF file to room with or without personalized message '''
    base_url = "https://webexapis.com"
    api_resource = "/v1/messages"
    data = MultipartEncoder(
        {
            "roomId": room_id,
            "text": text,
            "files": (filename, open(filename, "rb"), "application/pdf")
        }
    )
    headers = {
        "Authorization": f"Bearer {bot_token}",
        "Content-Type": data.content_type
    }
    try:
        response = requests.post(
            f"{base_url}{api_resource}",
            headers=headers,
            data=data,
            verify=certifi.where()
        )
    except Exception as e:
        logger.exception(f"\nEXCEPTION: {e}\n\n")
        print(f"EXCEPTION: {e}")
        sys.exit(1)
    else:
        if response.ok:
            logger.info(f"\nSUCCESS: send file to room\n")
            logger.debug(f"\nDETAILS: {room_name} ({filename})\n")
            print(F"SUCCESS: {room_name} ({filename})")
        else:
            logger.error("\nFAILURE: send file to room\n")
            print(
                f"[{response.status_code}]",
                json.loads(response.text).get("message")
            )
            sys.exit(1)        


def archive_file(file):
    ''' Move posted .PDF file to archive folder '''
    timestamp = datetime.datetime.now().strftime("%m-%d-%Y_%H-%M-%S")
    newfile = f"./archive/{timestamp}_{file.split('.')[0]}.pdf"
    os.replace(file, newfile)
    logger.info(f"\nSUCCESS: archive file\n")
    logger.debug(f"\nDETAILS: {file} -> {newfile}\n")
    print(f"Archived: {file} -> {newfile}")


def distribute_files(text, pdf_filenames, rooms, bot_token):
    ''' Distribute .PDF file(s) to Webex Team Room(s) '''
    if pdf_filenames is None:
        for room in rooms:
            room_id = room.get("id")
            room_name = room.get("title")
            if room.get("type") != "group":
                continue
            if text and room.get("type") == "group":
                post_text(room_id, room_name, bot_token, text=text)
            else:
                post_text(room_id, room_name, bot_token)
    else:
        for file in pdf_filenames:
            for room in rooms:
                room_id = room.get("id")
                room_name = room.get("title")
                if room.get("type") != "group":
                    continue
                if text and room.get("type") == "group":
                    post_text_pdf(file, room_id, room_name, bot_token, text=text)
                else:
                    post_text_pdf(file, room_id, room_name, bot_token)
            archive_file(file)


# RUN APPLICATION
if __name__ == "__main__":
    bots = get_bots()
    bot_token = get_bot_token(bots)
    action = get_action()
    if action == 'text':
        pdf_filenames = None
    elif action == 'text-pdf':
        pdf_filenames = get_pdfs()
    text = get_personal_msg()
    rooms = get_rooms(bot_token)
    distribute_files(text, pdf_filenames, rooms, bot_token)
